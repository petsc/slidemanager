import os
import sys
import subprocess
import os
import re
import math
import tempfile

from PIL import Image, ImageTk
import tkinter as tk
from tkinter import filedialog

# maps from each canvas to the underlying slide
id_to_object1 = {}
id_to_object3 = {}
slides = []
scale = .85  # scale for Barry's laptop
scale = 1.8   # scale for Barry's monitor
smallwidth = int(scale*396)
largewidth = int(scale*592)
largeheight = int(scale*612)
height = int(scale*700)
smallheight = int(.77*smallwidth)  # .77 is the aspect ratio of the images that convert produces
totalwidth = 100+2*smallwidth + largewidth
totalheight = 100+height
count1 = 0
count3 = 0

# Create the root window
root = tk.Tk()
root.minsize(totalwidth,totalheight)
root.geometry(str(totalwidth)+"x"+str(totalheight))
root.lift ()
root.call('wm', 'attributes', '.', '-topmost', True)
root.after_idle(root.call, 'wm', 'attributes', '.', '-topmost', False)
    
frame1=tk.Frame(root,width=smallwidth,height=height)
frame1.grid(row=0,column=0)
frame2=tk.Frame(root,width=largewidth,height=height)
frame2.grid(row=0,column=1)
frame3=tk.Frame(root,width=smallwidth,height=height)
frame3.grid(row=0,column=2)

frametop = tk.Frame(frame2)
frametop.pack()

searchframe = tk.Frame(frametop)
searchframe.grid(row=0,column=0)
searchlabel = tk.Label(searchframe,text='')
searchlabel.grid(row=0,column=0)
search = tk.Entry(searchframe)
search.grid(row=0,column=1)
def mysearch(event):
  DisplayAll(search.get())
search.bind('<Return>', (lambda e: mysearch(e)))

#retainslides = tk.IntVar()
#_retainslides = tk.Checkbutton(frametop, text="Keep current slides", variable=retainslides)
#_retainslides.grid(row=0,column=1)

loadfilebutton = tk.Button(frametop,text='Load file')
loadfilebutton.grid(row=0,column=1)
def myloadfile(event):
  filename = tk.filedialog.askopenfilename(initialdir='.')
  if filename:
    ReadAll(filename)
    DisplayAll('')
loadfilebutton.bind('<Button-1>', (lambda e: myloadfile(e)))

loaddirbutton = tk.Button(frametop,text='Load dir')
loaddirbutton.grid(row=0,column=2)
def myloaddir(event):
  filename = tk.filedialog.askdirectory(initialdir='.')
  if filename:
    ReadAll(filename)
    DisplayAll('')
loaddirbutton.bind('<Button-1>', (lambda e: myloaddir(e)))

canvas1 = tk.Canvas(frame1, highlightthickness=10)
scrollbar1 = tk.Scrollbar(frame1,orient=tk.VERTICAL)
scrollbar1.pack(side=tk.LEFT, fill=tk.Y)
scrollbar1.config(command=canvas1.yview)
canvas1.config(width=smallwidth,height=height)
canvas1.config(yscrollcommand=scrollbar1.set,yscrollincrement=25)
canvas1.pack(side=tk.LEFT,expand=True,fill=tk.BOTH)

#
#  This generates the error in Root.mainloop() hence the try there
#
# UnicodeDecodeError: 'utf-8' codec can't decode byte 0xff in position 0: invalid start byte
def _on_mousewheel(event,canvas):
  try:
    canvas.yview('scroll',event.delta,tk.UNITS)
  except:
    #raise RuntimeError()
    pass
canvas1.bind("<MouseWheel>", lambda e: _on_mousewheel(e,canvas1))


canvas2 = tk.Canvas(frame2, highlightthickness=10)
canvas2.config(width=largewidth,height=height)
canvas2.pack(side=tk.LEFT,expand=True,fill=tk.BOTH)

saveframe = tk.Frame(frame3)
saveframe.pack()
acceptbutton = tk.Button(saveframe,text='Accept all')
acceptbutton.grid(row=0,column=0)
def myacceptbutton(event):
  '''Moves all slides display on the left panel to the right panel'''
  for keys,i in id_to_object1.items():
    i.DisplayRight()
acceptbutton.bind('<Button-1>', (lambda e: myacceptbutton(e)))
savelabel = tk.Label(saveframe,text='Save slide list')
savelabel.grid(row=0,column=1)
save = tk.Entry(saveframe)
save.grid(row=0,column=2)

def mysave(event):
  '''Save in the given file the list of all current right hand side slides'''
  global id_to_object3
  fd = open(save.get(),'w')
  for keys,i in id_to_object3.items():
    fd.write(i.filename+'\n')
  fd.close()
save.bind('<Return>', (lambda e: mysave(e)))

canvas3 = tk.Canvas(frame3, highlightthickness=10)
scrollbar3 = tk.Scrollbar(frame3,orient=tk.VERTICAL)
scrollbar3.pack(side=tk.LEFT, fill=tk.Y)
scrollbar3.config(command=canvas3.yview)
canvas3.config(width=smallwidth,height=height)
canvas3.config(yscrollcommand=scrollbar3.set,yscrollincrement=25)
canvas3.pack(side=tk.LEFT,expand=True,fill=tk.BOTH)
canvas3.bind("<MouseWheel>", lambda e: _on_mousewheel(e,canvas3))

def drag(event, canvas):
  canvas.tag_raise(tk.CURRENT)
  x = canvas.canvasx(event.x)
  y = canvas.canvasy(event.y)
  canvas.coords(tk.CURRENT, smallwidth/2,y)
  if event.y > 660:
    canvas.yview_scroll(2,tk.UNITS)  
  if event.y < 0:
    canvas.yview_scroll(-2,tk.UNITS)  
  # two ways of getting the current object unused
  id = canvas.find_closest(x,y)[0]
  id = canvas.find_withtag(tk.CURRENT)[0]

def dragend(event, canvas):
  global id_to_object3,count3,scrollbar3
  x = canvas.canvasx(event.x)
  y = canvas.canvasy(event.y) + smallheight/2.0
  cl = scrollbar3.get()
  locationid = int(math.floor(y/smallheight)) - 1 # id of slide we a sitting on top of
  id = -100000
  for t in canvas.gettags(tk.CURRENT):
    if t.startswith('T'):
      id = int(t[1:])
  # clear third canvas
  oldmap = id_to_object3
  canvas.delete('all')
  id_to_object3 = {}
  count3 = 0
  if locationid ==  - 1:
    oldmap[id].DisplayRight()
  for key,i in oldmap.items():
    if key == locationid:
      i.DisplayRight()
      oldmap[id].DisplayRight()      
    elif not key == id:
      i.DisplayRight()
  # this doesn't seem to do anything    
  #scrollbar3.set(cl[0],cl[1])
  canvas.yview_moveto(cl[0])
      
large_slide = None
def enlarge(event, canvas):
  global large_slide,canvas2
  x = canvas.canvasx(event.x)
  y = canvas.canvasy(event.y)
  locationid = int(math.floor(y/smallheight))
  large_slide = id_to_object1[locationid]
  canvas2.create_image(largewidth/2,largeheight/2,image=large_slide.image_large,tag='tag_large')
  canvas2.tag_bind('tag_large', '<Button-2>', lambda e: edit(e, canvas2))
  canvas2.tag_bind('tag_large', '<Button-1>', lambda e: save_to_right(e))  
def enlarge3(event, canvas):
  global large_slide,canvas2
  x = canvas.canvasx(event.x)
  y = canvas.canvasy(event.y)
  locationid = int(math.floor(y/smallheight))
  large_slide = id_to_object3[locationid]
  canvas2.create_image(largewidth/2,largeheight/2,image=large_slide.image_large,tag='tag_large')
  canvas2.tag_bind('tag_large', '<Button-2>', lambda e: edit(e, canvas2))
  canvas2.tag_bind('tag_large', '<Button-1>', lambda e: save_to_right(e))  

#  Duplicate the currently large slide
duplicatebutton = tk.Button(frametop,text='Duplicate')
duplicatebutton.grid(row=0,column=3)
def duplicate(event):
  ''' Duplicates the current large slide'''
  global large_slide,canvas2,id_to_object3,canvas3,slides,count3
  cmd = 'cp '+large_slide.filename+' '+large_slide.filename.replace('.tex','_D.tex')
  proc = subprocess.Popen(cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE,shell=True)
  proc.wait()
  a = Slide(large_slide.filename.replace('.tex','_D.tex'))
  a.GenerateImage()
  if a.image_small:
    slides.append(a)
    a.DisplayLeft()
    canvas2.create_image(largewidth/2,largeheight/2,image=a.image_large,tag='tag_large')
    canvas2.tag_bind('tag_large', '<Button-2>', lambda e: edit(e, canvas2))
    canvas2.tag_bind('tag_large', '<Button-1>', lambda e: save_to_right(e))
    #  this code duplicated from dragend
    if hasattr(large_slide,'count3'):
      cl = scrollbar3.get()
      # clear third canvas
      oldmap = id_to_object3
      canvas3.delete('all')
      id_to_object3 = {}
      count3 = 0
      large_slide_count3 = large_slide.count3
      for k,i in oldmap.items():
        i.DisplayRight()
        if k == large_slide_count3:
          a.DisplayRight()
      canvas3.yview_moveto(cl[0])
    large_slide = a
duplicatebutton.bind('<Button-1>', (lambda e: duplicate(e)))

newslidebulletbutton = tk.Button(frametop,text='New Template')
newslidebulletbutton.grid(row=1,column=0)
def newslidebullet(event):
  global large_slide,canvas2,slides
  a = tempfile.NamedTemporaryFile(suffix='.tex', prefix='newslide-', dir='slides', delete=False)
  a.close()
  filename = os.path.join('slides',a.name)
  a = open(filename,'w')
  a.write('\\begin{frame}{New Slide with Bulleted List:}\n\\begin{itemize}\n\\item\n\\item\n\\end{itemize}\n\\includegraphics[width=\\textwidth]{figures/DummyFigure.png}\n\\begin{align*}\n    F(u)=0 \\quad &\\sim\\quad -\\div\\big[ (1+u^2) \\nabla u \\big] - f = 0 \\\\\n  J(u)w \\quad &\\sim\\quad  -\\div\\big[(1+u^2)\\nabla w + 2uw\\nabla u \\Big]\n\\end{align*}\n\\end{frame}')
  a.close()
  a = Slide(filename)
  a.GenerateImage()
  if a.image_small:
    slides.append(a)
    a.DisplayLeft()
    canvas2.create_image(largewidth/2,largeheight/2,image=a.image_large,tag='tag_large')
    canvas2.tag_bind('tag_large', '<Button-2>', lambda e: edit(e, canvas2))
    canvas2.tag_bind('tag_large', '<Button-1>', lambda e: save_to_right(e))
  large_slide = a    
newslidebulletbutton.bind('<Button-1>', (lambda e: newslidebullet(e)))


newslidebutton = tk.Button(frametop,text='New')
newslidebutton.grid(row=1,column=1)
def newslide(event):
  global large_slide,canvas2,slides
  a = tempfile.NamedTemporaryFile(suffix='.tex', prefix='newslide-', dir='slides', delete=False)
  a.close()
  filename = os.path.join('slides',a.name)
  a = open(filename,'w')
  a.write('\\begin{frame}{New slide:}\n\\end{frame}')
  a.close()
  a = Slide(filename)
  a.GenerateImage()
  if a.image_small:
    slides.append(a)
    a.DisplayLeft()
    canvas2.create_image(largewidth/2,largeheight/2,image=a.image_large,tag='tag_large')
    canvas2.tag_bind('tag_large', '<Button-2>', lambda e: edit(e, canvas2))
    canvas2.tag_bind('tag_large', '<Button-1>', lambda e: save_to_right(e))
  large_slide = a    
newslidebutton.bind('<Button-1>', (lambda e: newslide(e)))

#  Rerun pdflatex and generate new image for the currently large slide
reprocessbutton = tk.Button(frametop,text='Pdflatex')
reprocessbutton.grid(row=1,column=2)
def reprocess(event):
  ''' Reprocesss (rerun pdflatex) the current large slide'''
  global large_slide,canvas2,canvas1
  # reload tex from file to get users changes
  fd = open(large_slide.filename,'r')
  large_slide.text = fd.read()
  fd.close()
  large_slide.GenerateImage()
  if large_slide.image_small:
    canvas2.create_image(largewidth/2,largeheight/2,image=large_slide.image_large,tag='tag_large')
    canvas2.tag_bind('tag_large', '<Button-2>', lambda e: edit(e, canvas2))
    canvas2.tag_bind('tag_large', '<Button-1>', lambda e: save_to_right(e))
    #  create the displayed image for the left and right columns
    canvas1.create_image(smallwidth/2,(large_slide.count1+.5)*smallheight,image=large_slide.image_small,tags= 'tag_small')
    canvas1.tag_bind('tag_small', '<Button-2>', lambda e: enlarge(e, canvas1))
    if hasattr(large_slide,'count3'):
      tag = 'T'+str(large_slide.count3)
      canvas3.create_image(smallwidth/2,(large_slide.count3+.5)*smallheight,image=large_slide.image_small,tags=tag)
      canvas3.tag_bind(tag, '<Button-2>', lambda e: enlarge3(e, canvas3))
      canvas3.tag_bind(tag, '<B1-Motion>', lambda e: drag(e, canvas3))
      canvas3.tag_bind(tag, '<ButtonRelease-1>', lambda e: dragend(e, canvas3))    
reprocessbutton.bind('<Button-1>', (lambda e: reprocess(e)))
	
#  delete the large slide from the right hand side
deletebutton = tk.Button(frametop,text='Delete')
deletebutton.grid(row=1,column=3)
def delete(event):
  ''' Deletes the current large slide from the right hand side'''
  global large_slide,canvas2,id_to_object3,canvas3,count3
  if hasattr(large_slide,'count3'):
    cl = scrollbar3.get()
    # clear third canvas
    oldmap = id_to_object3
    canvas3.delete('all')
    id_to_object3 = {}
    count3 = 0
    large_slide_count3 = large_slide.count3
    for k,i in oldmap.items():
      if not k == large_slide_count3:
        i.DisplayRight()
    canvas3.yview_moveto(cl[0])
deletebutton.bind('<Button-1>', (lambda e: delete(e)))

def edit(event, canvas):
  '''Opens given tex slide file in Emacs'''
  global large_slide
  cmd = 'open '+large_slide.filename
  proc = subprocess.Popen(cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE,shell=True)
  proc.wait()

def save_to_right(event):
  global large_slide
  large_slide.DisplayRight()


class Slide():
  '''Create a latex slide either from a file or or from a string'''
  def __init__(self, text):
    if os.path.isfile(text):
      fd = open(text,'r')
      self.text = fd.read()
      fd.close()
      self.filename = text
    else:
      self.text = text
      self.filename = None
    self.hasframe = False
    if self.text.find('\\begin{frame}') > -1: self.hasframe = True
    if self.text.find('\\frame') > -1: self.hasframe = True
    return

  def GenerateImage(self):
    '''Run pdflatex on the slide'''
    self.image_small = None
    self.texfile = self.filename.replace('.tex','-t.tex')
    self.pdffile = self.texfile.replace('.tex','.pdf')
    self.jpgfile = self.pdffile.replace('.pdf','.jpg')
    self.jpgfile0 = self.jpgfile.replace('.jpg','-0.jpg')
    if os.path.isfile(self.jpgfile0):
      if os.path.getmtime(self.jpgfile0) < os.path.getmtime(self.filename):
        os.unlink(self.jpgfile0)
    badfile = self.pdffile.replace('.pdf','.bad')
    if os.path.isfile(badfile):
      if os.path.getmtime(badfile) < os.path.getmtime(self.filename):
        os.unlink(badfile)
    if os.path.isfile(badfile):
      self.jpgfile0 = 'figures/failedslide.jpg'
    if not os.path.isfile(self.jpgfile0):
      fd = open(self.texfile,'w')
      fd.write('\\documentclass[dvipsnames]{beamer}\n\\input{talkPreamble}\n\\usepackage{pgfpages}\n\\pgfpagesuselayout{resize to}[letterpaper,border shrink=5mm,landscape]\n\\begin{document}\n')
      if not self.hasframe: fd.write('\\begin{frame}\n')
      fd.write(self.text.replace('<testing>',''))
      if not self.hasframe: fd.write('\n\\end{frame}\n')
      fd.write('\n\\end{document}')
      fd.close()
      cmd = 'TEXINPUTS=./tex:${TEXINPUTS} pdflatex -halt-on-error -shell-escape '+self.texfile
      proc = subprocess.Popen(cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE,shell=True)
      try:
        proc.wait(timeout=15)
      except:
        err = 'Time out'
        out = ' '
      else:
        out = proc.stdout.read().decode('utf-8')
        err = proc.stderr.read().decode('utf-8')
        if err == "libpng warning: iCCP: Not recognizing known sRGB profile that has been edited\n":
          err = None
      if err or  proc.returncode < 0 or  out.find('Fatal error occurred') > -1 or not os.path.isfile(os.path.basename(self.pdffile)):
        print('Error compiling '+self.texfile)
        fd = open(badfile,'w')
        fd.write('Error running pdflatex '+out+err)
        fd.close()
        self.pdffile  = None
        self.jpgfile  = None
        self.jpgfile0 = 'figures/failedslide.jpg'
        print("Stdout");
        print(out)
        print("Stderr");
        print(err)
      else:
        cmd = 'mv '+os.path.basename(self.pdffile)+' '+os.path.dirname(self.pdffile)
        proc = subprocess.Popen(cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE,shell=True)
        proc.wait()
        cmd = 'convert '+self.pdffile+' '+self.jpgfile
        proc = subprocess.Popen(cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE,shell=True)
        proc.wait()
        if not os.path.isfile(self.jpgfile0):	
          cmd = 'mv '+self.jpgfile+' '+self.jpgfile0
          proc = subprocess.Popen(cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE,shell=True)
          proc.wait()

    global smallwidth,smallheight

    imagelarge = Image.open(self.jpgfile0)
    imagelarge = imagelarge.resize((largewidth,largewidth), Image.ANTIALIAS)
    self.image_large = ImageTk.PhotoImage(imagelarge)
    imagesmall = Image.open(self.jpgfile0)
    imagesmall.thumbnail((smallwidth,smallheight), Image.ANTIALIAS)
    self.image_small = ImageTk.PhotoImage(imagesmall)

  def DisplayLeft(self):
    global count1,canvas1,id_to_object1
    self.count1 = count1
    canvas1.create_image(smallwidth/2,(count1+.5)*smallheight,image=self.image_small,tags= 'tag_small')
    count1 = count1 + 1
    canvas1.tag_bind('tag_small', '<Button-2>', lambda e: enlarge(e, canvas1))
    id_to_object1[self.count1] = self

  def DisplayRight(self):
    global count3,canvas3,id_to_object3
    for keys,i in id_to_object3.items():
      if i == self: return
    self.count3 = count3
    tag = 'T'+str(self.count3)
    canvas3.create_image(smallwidth/2,(count3+.5)*smallheight,image=self.image_small,tags=tag)
    count3 = count3 + 1
    canvas3.tag_bind(tag, '<Button-2>', lambda e: enlarge3(e, canvas3))
    canvas3.tag_bind(tag, '<B1-Motion>', lambda e: drag(e, canvas3))
    canvas3.tag_bind(tag, '<ButtonRelease-1>', lambda e: dragend(e, canvas3))
    id_to_object3[self.count3] = self
    a = canvas3.bbox("all")
    # if I don't use the 1.3 business then the scrollbar marker goes off the bottom of the window before the last slide is displayed
    a = (a[0],a[1],a[2],int(1.3*a[3]))
    canvas3.configure(scrollregion = a)

def processDir(dirname,names):
  '''Generates images for all valid slide files in a directory'''
  global count1,slides
  for n in names:
    if n.endswith('.tex'):
      if n.endswith('-t.tex'): continue
      n = os.path.join(dirname,n)
      a = Slide(n)
      a.GenerateImage()
      if a.image_small:
        slides.append(a)

def ReadAll(arg):
  '''Reads in all files from the subdirectories of slides or reads in a list of files'''
  global id_to_object1,count1,id_to_object3,count3,slides,canvas2,canvas3,retainslides
  if not 1: #retainslides.get():
    id_to_object1 = {}
    id_to_object3 = {}
    slides = []
    count1 = 0
    count3 = 0
    canvas2.delete('all')
    canvas3.delete('all')    
  
  if not arg:
    arg = 'slides'
  if os.path.isfile(arg):
    fd = open(arg)
    files = fd.read().split('\n')
    fd.close()
    for f in files:
      processDir(os.path.dirname(f),[os.path.basename(f)])
  else:
    for (dirpath, dirnames, filenames) in os.walk(arg):
      processDir(dirpath,filenames)

def DisplayAll(text):
  '''Displays all files that contain a given string'''
  global canvas1,id_to_object1,count1,slides
  canvas1.delete('all')
  id_to_object1 = {}
  count1 = 0
  for s in slides:
    if text.lower() == text:
      if re.search(text, s.text, re.IGNORECASE):
        s.DisplayLeft()
    elif s.text.find(text) > -1:
      s.DisplayLeft()    
  a = canvas1.bbox("all")
  print(a)
  # if I don't use the 1.3 business then the scrollbar marker goes off the bottom of the window before the last slide is displayed
  a = (a[0],a[1],a[2],int(1.3*a[3]))
  canvas1.configure(scrollregion = a)

def SVGToPDF():
  '''Generates a pdf file for each svg file it finds in the figures directory'''
  cwd = os.getcwd()
  for (dirpath, dirnames, filenames) in os.walk('figures'):
    for filename in filenames:
      if filename.endswith('.svg') and not os.path.isfile(os.path.join(cwd,dirpath,filename.replace('.svg','.pdf'))):
        cmd = '/Applications/Inkscape.app/Contents/Resources/bin/inkscape  --export-pdf '+os.path.join(cwd,dirpath,filename.replace('.svg','.pdf'))+' '+os.path.join(cwd,dirpath,filename)
        print(cmd)
        proc = subprocess.Popen(cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE,shell=True)
        proc.wait()
      
if __name__ == '__main__':
  SVGToPDF()
  if len(sys.argv) > 1: arg = sys.argv[1]
  else: arg = None
  ReadAll(arg)
  DisplayAll('')
  ret = True
  while (ret):
   try:
     ret = root.mainloop()
   except UnicodeDecodeError:
     ret = True