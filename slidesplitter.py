import os
import sys
import subprocess
import os
import re
import shutil
  


def Split(dirname,filename):
  '''Reads in a files which may contain multiple slides and produces a file for each slide'''
  fd = open(os.path.join(dirname,filename))
  lines = fd.read().split('\n')
  fd.close()
  cnt = 0
  fd = None
  for l in lines:
    if l.startswith('\\begin{frame}'):
      fname = os.path.join('all-slides',filename.replace('.tex','-slide-'+str(cnt)+'.tex'))
      if os.path.isfile(fname): print('File already exists: '+fname)
      fd = open(fname,'w')
      cntl = 0
    if fd:
      fd.write(l+'\n')
      cntl = cntl+1
    if l.startswith('\\end{frame}'):
      cnt = cnt + 1
      if not fd:
         print('Found an end frame without begin: '+os.path.join(dirname,filename))
      else:
        fd.close()
        if cntl < 4: os.unlink(fname)
        fd = None
      
      

if __name__ == '__main__':
  if not os.path.isdir('all-slides'): os.mkdir('all-slides')
  for (dirpath, dirnames, filenames) in os.walk(sys.argv[1]):
    if dirpath.endswith('all-slides'): continue
    for f in filenames:
      if f.endswith('.tex'):
        Split(dirpath,f)