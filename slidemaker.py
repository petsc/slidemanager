import os
import sys
import subprocess
import os
import re
import math
import tempfile



def CreateSlides(filename):
  texfile = filename.replace('.txt','.tex')
  pdffile = filename.replace('.txt','.pdf')  
  fd = open(texfile,'w')
  fd.write('\\documentclass[dvipsnames]{beamer}\n\\input{talkPreamble}\n\\begin{document}\n')
  fs = open(filename)
  names = fs.read().split('\n')
  fs.close()
  for name in names:
    if name:
      fd.write('\input{'+name+'}\n')
  fd.write('\n\\end{document}')
  fd.close()
  cmd = 'TEXINPUTS=./tex:${TEXINPUTS} pdflatex -halt-on-error -shell-escape '+texfile
  proc = subprocess.Popen(cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE,shell=True)
  try:
    proc.wait(timeout=150)
  except:
    err = 'Time out'
    out = ' '
  else:
    out = proc.stdout.read().decode('utf-8')
    err = proc.stderr.read().decode('utf-8')
  if err or  proc.returncode < 0 or  out.find('Fatal error occurred') > -1 or not os.path.isfile(pdffile):
    print('Error compiling '+texfile+out+err)

if __name__ == '__main__':
  CreateSlides(sys.argv[1])
